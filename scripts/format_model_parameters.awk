#!/usr/bin/awk -f
### USAGE
## run: ./format_model_parameters.awk params.dat
# params.dat: text output of a running the angmom_suite proj program

BEGIN {OFS=" & "; ORS=" \\\\\n"}
{
    # parse parameter label
    args_regex = "[A-Za-z]+=[-'A-Za-z0-9]+";
    if (match($0, "\\(("args_regex", )*("args_regex")?\\)")){
        param = substr($0,0,RSTART-1)
        delete args;
        delete kwargs;
        num = split(substr($0,RSTART+1,RLENGTH-2), args, ", ");
        for (key in args) {
            # print key, args_array[key]
            split(args[key], key_value, "=")
            kwargs[key_value[1]] = key_value[2]
        }

        cart2sph["x"] = 1; cart2sph["y"] = -1; cart2sph["z"] = 0
        # cart2sph["x"] = "+1"; cart2sph["y"] = "-1"; cart2sph["z"] = "0"
        sph2cart[1] = "x"; sph2cart[-1] = "y"; sph2cart[0] = "z"
        # sph2cart["+1"] = "x"; sph2cart["-1"] = "y"; sph2cart["0"] = "z"

        ### Parse model parameters and generate operator label
        # TODO: adjust and extend by problem specific parameters/operators
        if (param == "shift") {
            operator = "identity"; parameter = "shift";
        }
        else if (param == "lamb") {
            if (num == 0) {  # isotropic SOC
                operator = "$\\hat{L} \\dot \\hat{S}$"; parameter = "$\\lambda$";
            } else {  # anisotropic SOC
                component = substr(kwargs["component"], 2, 1)
                operator = "$\\hat{L}_"component" \\hat{S}_"component"$"; parameter = "$\\lambda_"component"$";
            }
        }
        else if (param == "B") {
            operator = sprintf("$\\hat{O}_{%d}^{%+d}$", kwargs["k"], kwargs["q"]);
            parameter = sprintf("$B_{%d}^{%+d}$", kwargs["k"], kwargs["q"]);
        }
        else if (param == "J") {
            if (num == 3) {  # RS bipartite terms
                R_cart = substr(kwargs["alpha"], 2, 1)
                R_sph = cart2sph[R_cart]
                if (kwargs["k"] == 1) {  # k = 1
                    S_cart = sph2cart[kwargs["q"]];
                    S_op = sprintf("\\hat{S}_%s", S_cart);
                } else {  # k > 1
                    S_op = sprintf("\\hat{O}_{%d}^{%+d}", kwargs["k"], kwargs["q"]);
                }
                operator = "$\\hat{R}_"R_cart" "S_op"$";
                parameter = sprintf("$J^{RS}_{%+d, %d, %+d}$", R_sph, kwargs["k"], kwargs["q"]);
            }
            else if (num == 5) {  # RSL tripartite terms
                R_cart = substr(kwargs["alpha"], 2, 1)
                R_sph = cart2sph[R_cart]

                if (kwargs["k"] == 1) {  # k = 1
                    S_cart = sph2cart[kwargs["q"]];
                    S_op = "\\hat{S}_"S_cart;
                } else {  # k > 1
                    S_op = sprintf("\\hat{O}_{%d}^{%+d}", kwargs["k"], kwargs["q"]);
                }

                if (kwargs["n"] == 1) {  # n = 1
                    L_cart = sph2cart[kwargs["m"]];
                    L_op = "\\hat{L}_"L_cart;
                } else {  # k > 1
                    L_op = sprintf("\\hat{O}_{%d}^{%+d}", kwargs["n"], kwargs["m"]);
                }

                operator = "$\\hat{R}_"R_cart" "S_op" "L_op"$";
                parameter = sprintf("$J^{RSL}_{%+d, %d, %+d, %d, %+d}$", R_sph, kwargs["k"], kwargs["q"], kwargs["n"], kwargs["m"]);
            }
        }
        # compile table containing: parameter label + operator label + parameter value
        print parameter, operator , $NF
    }
}
