#!/usr/bin/awk -f
### USAGE
## run: ./format_state_data.awk composition.log gvalues.dat
# composition.log: contains energy + <Jz> + composition table from angmom_suite proj or model programs
# gvalues.dat: text output of a running angmom_suite eprg --eprg_values

function join(array, sep) {
    for (key in array)
        if (key == 1) {
            result = array[key]
        } else {
            result = result sep array[key]
        }
    return result
}

## CHANGE ME:
# OFS: field separator (default: '&' for latex tables)
# ORS: record separator (default: linux \n newline character)
# max_state: maximum state to include in output
# doublet flag: print records by doublet, useful for Kramers systems (default: True)
BEGIN {OFS=" & "; ORS=" \\\\\n"; max_state=16; doublet_flag=1}

### Parse output (Modify with care or you get what you deserve!)
# fetch compositions
FILENAME == ARGV[1] && /Diagonal basis energy, <S_z>, <L_z>, <J_z> and composition:/ {state_comp_flag=1;line_count=0;next}
FILENAME == ARGV[1] && /------------------------/ {if (state_comp_flag == 1 && line_count == 0) {line_count+=1} else {state_comp_flag=0; line_count=0};next}
FILENAME == ARGV[1] && state_comp_flag == 1 && line_count == 1 {
    # parses pattern like 99.64% |5/2, 5, 15/2, -15/2> 
    comp_regex = "[0-9][0-9]?\\.[0-9][0-9]% \\|([-+/0-9]+, )+[-+/0-9]+>"
    if (match($0, ": ("comp_regex"  \\+  )*"comp_regex"")) {
        ener[$2] = $3
        jz[$2] = $6
        comp_str = substr($0,RSTART+1,RLENGTH);
        patsplit(comp_str, comp_splt, /[0-9][0-9]?\.[0-9][0-9]% \|([-+/0-9]+, )+[-+/0-9]+>/)
        delete comp_fmt
        for (key in comp_splt) {
            comp_fmt[key] = "$"gensub(/\|([-+/0-9]+, )+([-+/0-9]+)>/, "\\\\ket{\\2}", "g", comp_splt[key])"$"
            sub(/%/, "\\%", comp_fmt[key])
        }
        comp[$2] = join(comp_fmt, ", ")
    }
}
# fetch G-values
FILENAME == ARGV[2] && $1 ~ /#/ {if ($2 == "eprg_values") {doublet=substr($3, 2, length($3)-2); eprg_flag=1; comment_count=1} else {comment_count+=1};next}
FILENAME == ARGV[2] && eprg_flag == 1 && comment_count == 4 {
    eprg_fmt = sprintf("%.2f", sqrt($1))
    if (length(eprg[doublet]) == 0) {
        eprg[doublet] = eprg_fmt
    } else {
        eprg[doublet] = eprg[doublet]" & "eprg_fmt
    }
}

### Print pretty table ###
# CHANGE ME: Customise for preferred table layout
END {
    if (doublet_flag == 0) {  # print properties for each SOC state
        for (state in ener) {
            if (state <= max_state) {
                doublet = int((state - 1) / 2) + 1
                print ener[state], jz[state], eprg[doublet], comp[state]
            }
        }
    } else {  # print properties by Kramers doublet
        for (doublet in eprg) {
            state = 2 * doublet  # second is the +mj state in positive field (or -+ ordering)
            # compile table containing: energy + <Jz> + G-values + angmom composition
            print ener[state],  gensub(/\+/, "\\\\pm ", "g", jz[state]), eprg[doublet], gensub(/\-/, "\\\\mp ", "g", gensub(/\+/, "\\\\pm ", "g", comp[state]))
        }
    }
}
