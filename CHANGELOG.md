# CHANGELOG


## v1.21.0 (2025-03-18)

### Features

- Allow turning off arrows in barrier fig
  ([`dd757d7`](https://gitlab.com/chilton-group/angmom_suite/-/commit/dd757d7722d53235a69be90b7b1e136d17961215))

- Update barrier to take hdf5 input
  ([`0aaa81d`](https://gitlab.com/chilton-group/angmom_suite/-/commit/0aaa81d0834b5df207b8e54670793cb192ce4537))


## v1.20.3 (2025-03-18)

### Bug Fixes

- Allow multiplicities which are not part of basis space
  ([`2b52bd4`](https://gitlab.com/chilton-group/angmom_suite/-/commit/2b52bd4c8a56bf56efef079782836a453cbc9ccd))

Allow the presence of ab initio states which are not present in the intermediate angular momentum
  basis (atomic term/level) space.

- Completely fixes previous issue
  ([`bbd3ca5`](https://gitlab.com/chilton-group/angmom_suite/-/commit/bbd3ca5edaed382160c9de5c36cbe6ffaae53c6f))


## v1.20.2 (2025-03-18)

### Build System

- Update sympy requirements
  ([`1001057`](https://gitlab.com/chilton-group/angmom_suite/-/commit/1001057fa5d3529d4b7f0bbb7787b17a035bf417))

- Update sympy requirements
  ([`94bea2f`](https://gitlab.com/chilton-group/angmom_suite/-/commit/94bea2fb800c2e64899e031958b0543156f9e0b0))

### Continuous Integration

- Update build variables
  ([`e73738a`](https://gitlab.com/chilton-group/angmom_suite/-/commit/e73738a0dd1ab24d09113d08ab098a60742162f0))


## v1.20.1 (2025-02-06)

### Bug Fixes

- Basis inconsistencies
  ([`60bb2a0`](https://gitlab.com/chilton-group/angmom_suite/-/commit/60bb2a021cde8020e58873aa3affd67f7f70a8ca))

Fixes changes introduced in commit 2621d08f. Also, enables system rotation in operator output of the
  model program.

### Continuous Integration

- (short term fix) specify PSR version
  ([`ee9ab45`](https://gitlab.com/chilton-group/angmom_suite/-/commit/ee9ab4504ffd1401472c25e91416e81fd15f0904))

- Debug config
  ([`5f23842`](https://gitlab.com/chilton-group/angmom_suite/-/commit/5f23842a3e4686e59d552f2fab3de4222ec3ab49))

- Update changelog location [skip-ci]
  ([`3cc33f3`](https://gitlab.com/chilton-group/angmom_suite/-/commit/3cc33f340bb3ec36d802b49d064addef7b6c567c))

### Documentation

- Update quax description
  ([`7c09b3d`](https://gitlab.com/chilton-group/angmom_suite/-/commit/7c09b3df80e98ad1c12ffac695bcdadd0283bd90))


## v1.20.0 (2024-10-02)

### Bug Fixes

- Bypass k_max > 12 error with warning
  ([`adf2cab`](https://gitlab.com/chilton-group/angmom_suite/-/commit/adf2cab1d6f352a6df2710f027a07e5f1f94b817))

The case of stevens operators with k > 12 is treated with a warning using only k <= 12 model
  operators.

- Coupling of angmom operators
  ([`24f5b8e`](https://gitlab.com/chilton-group/angmom_suite/-/commit/24f5b8e654b088951f974d8b0cc2378b2cd6a7a0))

On earlier commit on this branch skipped the coupling of the angmom operators (together with the
  quax rotation). Reinstate coupling

- Elementary operators
  ([`7da705e`](https://gitlab.com/chilton-group/angmom_suite/-/commit/7da705e2455d914abb219b2559eec1be1a12bd00))

Did not used to include lower case angmom symbols.

- Get op warning
  ([`1131404`](https://gitlab.com/chilton-group/angmom_suite/-/commit/1131404885474813aab8b730c0fb6419888761da))

- Introduce atol in WE reduction to capture zero MEs
  ([`22189ea`](https://gitlab.com/chilton-group/angmom_suite/-/commit/22189ea942bc2e931980b9afff34d316c9dadf8e))

In case of zero matrix elements the atol parameter needs to be loosened.

- Minor bug
  ([`8c9b802`](https://gitlab.com/chilton-group/angmom_suite/-/commit/8c9b802882e00ed33931c1f88772fa561c92d34a))

see code

- Minor susc bug
  ([`aba309c`](https://gitlab.com/chilton-group/angmom_suite/-/commit/aba309ca72c8c8e9a4a1dd829c6514000b77fac9))

Wrong variable name was referenced in unstaged susceptibility routine

- Term function ordering
  ([`1a78b3c`](https://gitlab.com/chilton-group/angmom_suite/-/commit/1a78b3c8ea42a186e5dbf1b3354130d52f90dcb6))

Term function ordering of CG-trafo needs to be fixed to avoid mismatch between ordering expected by
  term projection and trafo. Also refactors rotations.

### Features

- Add shift argument
  ([`7804eee`](https://gitlab.com/chilton-group/angmom_suite/-/commit/7804eee3218da434e27e18b7ab7c62f13c13503f))

Enables/disable automatic shift of energy eigenvalues in print-out.

- Balanced construction of basis
  ([`a847e13`](https://gitlab.com/chilton-group/angmom_suite/-/commit/a847e132aa98650f2e5523f0e7632bed9cc92740))

Instead of constructing basis sequentially through recursion, build it independently and
  orthonormalise in the end.

- Check orthogonality
  ([`d03657b`](https://gitlab.com/chilton-group/angmom_suite/-/commit/d03657bd75c8be9a3b7234d0635663c2337532bc))

Check orthogonality among model operators.

- Direct construction of vectors
  ([`212529d`](https://gitlab.com/chilton-group/angmom_suite/-/commit/212529db65a2450a8e208cc44c40f10608d2ff06))

Construct vectors from column of generalised projectors directly without going via
  orthogonalisation.

- General projection operators
  ([`7d846de`](https://gitlab.com/chilton-group/angmom_suite/-/commit/7d846de29a08d7b9c39216ab1839119eaaabd7ad))

Implementation of general projection operators of semi-simple Lie groups onto the highest weight
  vector.

- Generalised R3 projectors in integral form
  ([`b0bb19c`](https://gitlab.com/chilton-group/angmom_suite/-/commit/b0bb19c5b45c50b0c9ae2ff8aa1a69ceebb3968a))

Implementation of the generalised R3 projector (arbitrary weights) in integral form. More robust
  then heighest weight + ladder ops, but does not resolve all the problems.

- Infinitesimal algo (wip)
  ([`2d43b39`](https://gitlab.com/chilton-group/angmom_suite/-/commit/2d43b390f678355b11852628b8eefb4db598d930))

Implement infinitesimal algorithm for proj op onto heighest weight, does not work due to divergence
  probs from the denominator (I think).

- J basis construction
  ([`e7bd0cb`](https://gitlab.com/chilton-group/angmom_suite/-/commit/e7bd0cb6a39e29348ea56a1c5d1b3baa4e642781))

Refactor basis construction to accommodate constuction of ab initio total angular momentum basis.

- Ladder operators, wip
  ([`0ee81f9`](https://gitlab.com/chilton-group/angmom_suite/-/commit/0ee81f95266327d98f9ada1ce5cc597a94873e22))

- Loewdin infinitesimal form (wip)
  ([`fa983b7`](https://gitlab.com/chilton-group/angmom_suite/-/commit/fa983b7b5f11f6638eb359c29a692bad8f7615bc))

Does not work as intended

- Multiplet option for eprg tensor
  ([`be47eb5`](https://gitlab.com/chilton-group/angmom_suite/-/commit/be47eb58172e66c3931d55ab5729e51c28b8543f))

Implement option to specify multiplicity of low-lying states.

- Phase correction for j basis
  ([`6f4c7e3`](https://gitlab.com/chilton-group/angmom_suite/-/commit/6f4c7e3a62ef3a56ebc342fb318b19e35d6b2e44))

Correct relative phases of intermediate level basis via spin and angm operators.

- Project angmom functions one-by-one
  ([`570dddc`](https://gitlab.com/chilton-group/angmom_suite/-/commit/570dddc1e7b00861ec92d24c0f6e37bef97d5440))

Instead of projecting the whole irrep and then diagonalising Lz, angmom functions are projected out
  one-by-one.

- Robust angm function construction
  ([`c448a46`](https://gitlab.com/chilton-group/angmom_suite/-/commit/c448a46b8cbde175fa88d9d15e6f37ef6157f916))

Find largest eigenvector of projection operator (l, m, m) instead of computing vectors directly.
  Downside, slow and requires phasing.

- Truncation option
  ([`839bd66`](https://gitlab.com/chilton-group/angmom_suite/-/commit/839bd667f2d053a95f862d69084dd1db0131eb2e))

Currently only implemented for the j basis. Should be extended to the spin-free l basis.

- Zeeman option
  ([`cff7579`](https://gitlab.com/chilton-group/angmom_suite/-/commit/cff757900a01f1e204a5a5b38aa3811a160c857c))

Project zeeman operators.

### Refactoring

- Clean up changes from single function branch
  ([`a7fff1b`](https://gitlab.com/chilton-group/angmom_suite/-/commit/a7fff1b32dcc45293769148dab231ec1431597c7))

reverts back to irrep projection but keeps new redused HSO phasing algo.

- Efficient angmom operator construction
  ([`4bbebbf`](https://gitlab.com/chilton-group/angmom_suite/-/commit/4bbebbfcd08eff8f4babda6008a4fb75527a74bd))

Make construction of coupled angular momentum operators more efficient by utilising WE.

- Handling of zeeman model
  ([`9504684`](https://gitlab.com/chilton-group/angmom_suite/-/commit/9504684818bf6edb276c26d4113d2e6010a4ed40))

Factor out Zeeman modeling from SpinHamiltonian (only core Ham).

- Improve amfi phasing from H_so
  ([`cc32bd5`](https://gitlab.com/chilton-group/angmom_suite/-/commit/cc32bd50bf853ef91a9b7ebf48f5dccb693810c7))

use the spin and angm WE-reduced matrix elements of the amfi integrals and model H_so matrix element
  to adjust relative term phases.

- Improve we reduction
  ([`9fcdebf`](https://gitlab.com/chilton-group/angmom_suite/-/commit/9fcdebf78f955eebe2088ea211a840ba0bfc8f90))

Now takes into account exactly zero reduced matrix elements more explicitely and works on single
  tensor components at a time.

- Remove prints
  ([`3499602`](https://gitlab.com/chilton-group/angmom_suite/-/commit/34996027aa71e2aa2c635a39adb963e4275788ec))

- Term trafo, wip
  ([`68e180c`](https://gitlab.com/chilton-group/angmom_suite/-/commit/68e180c894c5a9afb48a0568a304e266078d5ed5))

- Treatment of quax and field arguments
  ([`2621d08`](https://gitlab.com/chilton-group/angmom_suite/-/commit/2621d08f54788c1eb461df525c03a0d7b6746a12))

quax and field have the same implication whether applied in the proj or model program. Quax rotates
  ab initio vector operators in the proj case and the Hamiltonian operator in the model case.


## v1.19.0 (2024-04-24)

### Continuous Integration

- Fix error in pyproject.toml
  ([`4fdc24c`](https://gitlab.com/chilton-group/angmom_suite/-/commit/4fdc24c20432beb000216c892679bd42bf7dbb63))

### Features

- Add scripts
  ([`57fe9e4`](https://gitlab.com/chilton-group/angmom_suite/-/commit/57fe9e4f78896ec14fd855a8b8d1ebdd2d175a4e))

Adds convenient scripts for table generation.


## v1.18.1 (2024-03-07)

### Bug Fixes

- Kramers doublet rotation
  ([`e23ed6d`](https://gitlab.com/chilton-group/angmom_suite/-/commit/e23ed6d591acdbaf1cd9a31ad6500569ae743b19))

Improves kramers doublet rotation for numerically equal energies.

- Spin, angm and jtot expectation values
  ([`e2de4c7`](https://gitlab.com/chilton-group/angmom_suite/-/commit/e2de4c7d39c4066a6435c49c6fdf78ba88e3f06a))

Reference ops instead of self.ops, quax rotation was not applied to operators used for expectation
  value print out. Did not effect model Hamiltonian.

### Build System

- Compatibility for newest jax library
  ([`8a0ca42`](https://gitlab.com/chilton-group/angmom_suite/-/commit/8a0ca425120b777f404dccad3e7d31ddc930d514))

### Continuous Integration

- Add documentation badge
  ([`88b9da6`](https://gitlab.com/chilton-group/angmom_suite/-/commit/88b9da67ac6611995a74068e73e4de734e5d5f56))


## v1.18.0 (2023-11-01)

### Bug Fixes

- --no-phase_flip argument and lower we-reduction threshold
  ([`c22ded1`](https://gitlab.com/chilton-group/angmom_suite/-/commit/c22ded14e6a31af9b77c40123a894d159112f195))

WE-reduction treshold too low for some AMFI operator cases.

- Hashability of Term class
  ([`609dfea`](https://gitlab.com/chilton-group/angmom_suite/-/commit/609dfeac09bbec3f202f3c12ae99d9e21148fe3f))

Implementing __eq__ removed hashability. Adding __hash__ restores it.

### Features

- Amfi phase diagnostic
  ([`b026d96`](https://gitlab.com/chilton-group/angmom_suite/-/commit/b026d968cd64709994a27c03736084a52663d32f))

Run AMFI phase diagnostic if more then one term is involved in ab initio space.

- We reduction
  ([`88f4f23`](https://gitlab.com/chilton-group/angmom_suite/-/commit/88f4f23655d88a2b07d34c9b8e7ee1a7f9e993a5))

add Wigner-Eckart reduction

### Refactoring

- Amfi phasing
  ([`c39c81f`](https://gitlab.com/chilton-group/angmom_suite/-/commit/c39c81f1743dc361385f96129c11049c7b415e0c))

Complete AMFI phasing including overhaul of term trafo.

- Amfi phasing
  ([`947b82b`](https://gitlab.com/chilton-group/angmom_suite/-/commit/947b82bab203ff4305b22a70cc11433bc97480ad))

wip


## v1.17.2 (2023-10-20)

### Bug Fixes

- K_max argument for model
  ([`aee9049`](https://gitlab.com/chilton-group/angmom_suite/-/commit/aee90493b5a1d7dc20e4556b52c3ad2caf18802f))

Now also fixes k_max for angmom_suite model.


## v1.17.1 (2023-09-29)

### Bug Fixes

- Include k_max
  ([`17c3e7a`](https://gitlab.com/chilton-group/angmom_suite/-/commit/17c3e7ab6a5db85ba80f5805b88eaea922d2b64b))

use k_max in spin Hamiltonian construction


## v1.17.0 (2023-09-08)

### Refactoring

- Remove deprecated jax features
  ([`82ba7bf`](https://gitlab.com/chilton-group/angmom_suite/-/commit/82ba7bff3ed83e0f83800cbf8aa27cef05b0e43d))

stevens ops implementation does not need to be jaxable at this point remove until needed.
  Automatically makes code compatible with newest jax version.


## v1.16.1 (2023-07-31)

### Bug Fixes

- One based index for --states argument
  ([`634ca69`](https://gitlab.com/chilton-group/angmom_suite/-/commit/634ca69cbc27cd454f90081fcb6b188793fd06bc))

Before was zero based index.


## v1.16.0 (2023-07-31)

### Features

- Implement tint program
  ([`2782606`](https://gitlab.com/chilton-group/angmom_suite/-/commit/2782606ab9167237fce4beefa33a8556ce89f755))

Program to compute the matrix elements of the magnetic dipole operator


## v1.15.0 (2023-07-26)

### Bug Fixes

- Bring back performance
  ([`32c1963`](https://gitlab.com/chilton-group/angmom_suite/-/commit/32c19630aa2e347202ea0394c97c099ca3810b33))

The algorithms for differential and conventional susc were accidentally swapped

### Features

- General lie group projection operator implementation (wip)
  ([`6c21c54`](https://gitlab.com/chilton-group/angmom_suite/-/commit/6c21c547d19c170dd153e0d6243f7789594a4a5a))

### Performance Improvements

- Manually batched version of sus function
  ([`b79aa85`](https://gitlab.com/chilton-group/angmom_suite/-/commit/b79aa851a621f6cd86a2affc67bb50887362b165))

Keeps memory use low. Slower than vmapped version.

- Optimisation of sus routine
  ([`66db6d3`](https://gitlab.com/chilton-group/angmom_suite/-/commit/66db6d3143e5944fed7a38f8b743c451f1039846))

Additional performance improvements for sus routine, including the use of ode techniques and matrix
  exponential identities. Still quite memory hungry.

### Refactoring

- Improve numerical stability
  ([`cc1c2e1`](https://gitlab.com/chilton-group/angmom_suite/-/commit/cc1c2e1453691dba2493049b79b202a657a78ba1))

Condition eigenvalues by shift to make them positive definite for stable evaluation of matrix
  exponential.


## v1.14.0 (2023-07-24)

### Bug Fixes

- Bring back performance
  ([`6f73ecd`](https://gitlab.com/chilton-group/angmom_suite/-/commit/6f73ecd225cd3b336a2ea8a241450ca24b56d5b5))

The algorithms for differential and conventional susc were accidentally swapped

### Features

- General lie group projection operator implementation (wip)
  ([`79e0285`](https://gitlab.com/chilton-group/angmom_suite/-/commit/79e0285c96f6ae02c8438812ce68029793d4bfe3))

### Performance Improvements

- Manually batched version of sus function
  ([`7ec5932`](https://gitlab.com/chilton-group/angmom_suite/-/commit/7ec593260023f9393d8f32a06729f6ee51d5d401))

Keeps memory use low. Slower than vmapped version.

- Optimisation of sus routine
  ([`16e9c9a`](https://gitlab.com/chilton-group/angmom_suite/-/commit/16e9c9a99d62dd1ab2ac08b8c5f75c448235ecd3))

Additional performance improvements for sus routine, including the use of ode techniques and matrix
  exponential identities. Still quite memory hungry.


## v1.13.5 (2023-07-18)

### Bug Fixes

- Print SO term composition
  ([`a85a8d2`](https://gitlab.com/chilton-group/angmom_suite/-/commit/a85a8d2802fbca4ffc0a34b89b02017492c0e1e2))

Forgot to specify the coupling for Levels.

### Refactoring

- Remove powder code and put back optimisations
  ([`c4aa24a`](https://gitlab.com/chilton-group/angmom_suite/-/commit/c4aa24a034c8dcdb2b1bcb4ebb20709ac28c775b))

Powder code not needed for iso sus. Optimisations for sus calc at multiple temps.


## v1.13.4 (2023-07-17)

### Bug Fixes

- Print SO term composition
  ([`6bda301`](https://gitlab.com/chilton-group/angmom_suite/-/commit/6bda3010d7fcd4bce42ea3f14703942cf0b09109))

Forgot to specify the coupling for Levels.


## v1.13.3 (2023-07-10)

### Bug Fixes

- Add group code
  ([`24b3312`](https://gitlab.com/chilton-group/angmom_suite/-/commit/24b3312d3e9f505e5a1ecb3d5925aea556f3cb3a))

Was previously omitted from commit.

- Coupling setup of level class
  ([`95e7409`](https://gitlab.com/chilton-group/angmom_suite/-/commit/95e7409d4349db86abba656ab1a0ddff1f5f5674))

Setup was broken in earlier commit setting J: (L, S) instead of J: ((L, None), (S, None)). Reverted
  it all together as it is being taken care of when calling the constructor. Printing error now if
  right coupling not present.

- Finite susceptibility tensor
  ([`6b61d0c`](https://gitlab.com/chilton-group/angmom_suite/-/commit/6b61d0ce46070db2f85f5f497eb9072e2f475966))

Now magnetisation is computed along the Cartesian axis along application of field in three separate
  evaluations.

- Powder susceptibility (WIP)
  ([`76684c5`](https://gitlab.com/chilton-group/angmom_suite/-/commit/76684c5dd93fed8dadbb81156ba6b6ce300d79f1))

### Refactoring

- Proper integration for powder averaging (wip)
  ([`bd6d4c8`](https://gitlab.com/chilton-group/angmom_suite/-/commit/bd6d4c8e7fa815929e7d0f4bae6b48035dc3b497))

Carry out integration over orientations for powder averaging.


## v1.13.2 (2023-07-07)

### Bug Fixes

- Coupling setup of level class
  ([`370ead1`](https://gitlab.com/chilton-group/angmom_suite/-/commit/370ead1ebfef844f0e7318db77b65ef28ff59082))

Setup was broken in earlier commit setting J: (L, S) instead of J: ((L, None), (S, None)). Reverted
  it all together as it is being taken care of when calling the constructor. Printing error now if
  right coupling not present.


## v1.13.1 (2023-07-05)

### Bug Fixes

- Add group code
  ([`40f2810`](https://gitlab.com/chilton-group/angmom_suite/-/commit/40f2810ed26840fce48d0033ccb49f2b94108dcc))

Was previously omitted from commit.


## v1.13.0 (2023-07-05)

### Features

- Term composition print-out
  ([`d478269`](https://gitlab.com/chilton-group/angmom_suite/-/commit/d47826933c17756a02c87219291d60869b3c5769))

Bring back SOC term composition print-out and include energies.


## v1.12.1 (2023-06-30)

### Bug Fixes

- Factor of 2 in G-matrix calculation
  ([`d6fd540`](https://gitlab.com/chilton-group/angmom_suite/-/commit/d6fd540d3e663da8208b1f27b863ed1445e01427))

Also cleans up output.

### Refactoring

- Clean up sus output
  ([`c417f68`](https://gitlab.com/chilton-group/angmom_suite/-/commit/c417f68f7dd90eda415ff5b944d2d00b58c72ba5))

Now comes with nice temperature annotations and corrected header.


## v1.12.0 (2023-03-31)

### Features

- Facilitate cfp reading from hdf5 sub-groups
  ([`207174b`](https://gitlab.com/chilton-group/angmom_suite/-/commit/207174b421a8926681a3ae14245e5ae83c9ce083))

### Refactoring

- Copmutation of spin/angm operators
  ([`b8c6b45`](https://gitlab.com/chilton-group/angmom_suite/-/commit/b8c6b454a65b3918b32cee8ecd19fa7a352167ce))

Now also works for levels etc.


## v1.11.1 (2023-03-07)

### Bug Fixes

- Bug in cg expansion
  ([`e98e15a`](https://gitlab.com/chilton-group/angmom_suite/-/commit/e98e15a09cd32984d2bf0f2fc3f2986709a279d9))

Wrong array size in phase adjustment after basis completion.

### Refactoring

- Rename rotate_angm to rotate_cart
  ([`0078e30`](https://gitlab.com/chilton-group/angmom_suite/-/commit/0078e30946d0f4453e86ac6c26fb2cdd27e79517))

rotates arbitrary cartesian operator. Now in .basis


## v1.11.0 (2023-02-20)


## v1.10.2 (2023-01-13)

### Bug Fixes

- Minor bug fixes introduced in this branch
  ([`5d911ad`](https://gitlab.com/chilton-group/angmom_suite/-/commit/5d911ad1d61173775d07ef344c136e048f7600a8))

Also set phase of amfi off-diagonal blocks positive.

- Oef now used if provided
  ([`76f01fd`](https://gitlab.com/chilton-group/angmom_suite/-/commit/76f01fdd71b087cec05a519615c28566341342f3))

### Build System

- Updated dependencies
  ([`6621012`](https://gitlab.com/chilton-group/angmom_suite/-/commit/6621012c77480981ebf762d0f8efd82220f8a1bb))

bump angmom suite + remove scipy version requirement

### Refactoring

- Amfi to soc transformation
  ([`5c55de6`](https://gitlab.com/chilton-group/angmom_suite/-/commit/5c55de6faba38410216a4b9ae79bf331fc6181ae))

Have sf operators play nice with basis trafos.

- Base operators on spin-free formalism
  ([`d9d3095`](https://gitlab.com/chilton-group/angmom_suite/-/commit/d9d3095f82583ad01b95bcc470f4bc3665b1c30d))

spin-free operator lists are introduced at early stage.

- Clean up model interface
  ([`a40540f`](https://gitlab.com/chilton-group/angmom_suite/-/commit/a40540f8a5edcd445a2b41a6b1273c7880267128))

Modularisation of model functionality. Out-source property calculation.

- Clean up model interface (wip)
  ([`6d240a8`](https://gitlab.com/chilton-group/angmom_suite/-/commit/6d240a842b3e276d166f71ec3b0615039387f124))

The model interface has been a "hot mess" ever since it was first implemented in a rush. Cleaning up
  now.

- Make jax compatible
  ([`c7b219a`](https://gitlab.com/chilton-group/angmom_suite/-/commit/c7b219a4b92e965e482a23f0513e93c888f4e0b6))

Also basis is reordered such that the relevant block comes first.

- Move to autodiff, wip
  ([`8ed6dc7`](https://gitlab.com/chilton-group/angmom_suite/-/commit/8ed6dc7a23744b22f148b23ec0b59f63e54d6708))

highly wip

- Unify backend
  ([`34c0ef4`](https://gitlab.com/chilton-group/angmom_suite/-/commit/34c0ef4c779ba6641d1a7886f1ba0b04142e0509))

Consistency between evaluate() functions and usage of sf quantities.


## v1.10.1 (2022-08-25)

### Bug Fixes

- Rename `stevenskq_indices`f
  ([`83bc9e0`](https://gitlab.com/chilton-group/angmom_suite/-/commit/83bc9e03f47cb759966a1193a2dca674195652e7))


## v1.10.0 (2022-08-19)

### Bug Fixes

- Correct units for g-tensor
  ([`58e259f`](https://gitlab.com/chilton-group/angmom_suite/-/commit/58e259f47d2282aa42659cdab9db839ecf5bf3f5))

- Minor bug fixes and clean up
  ([`a8e5439`](https://gitlab.com/chilton-group/angmom_suite/-/commit/a8e5439bf91ce5d64096d092e06cfecaf7f2767e))

- clean up SOC - add __contains__ member to Symbol class - improve usage of sign flip

- Omit large integers in Okq
  ([`43c65d2`](https://gitlab.com/chilton-group/angmom_suite/-/commit/43c65d242d6e51406d0063bead93dc763a9730f4))

Convert integer to float before power computation.

- Print term/state composition for each spin multiplicity
  ([`fef4a30`](https://gitlab.com/chilton-group/angmom_suite/-/commit/fef4a30ddf74803034c3fa4281a93c70e7a278a8))

- Remove quantisation field from chi model hamiltonian
  ([`e2151c1`](https://gitlab.com/chilton-group/angmom_suite/-/commit/e2151c1ed7eeb8d00a19277698b5c67b1806fbb2))

the quantisation field is separated from the model Hamitonian to obtain the bare zero-field
  Hamiltonian to be employed in the chi calculations.

- Remove row permutation
  ([`43a354e`](https://gitlab.com/chilton-group/angmom_suite/-/commit/43a354efc9ee045441bb394513fd4998888d59f4))

The rows of the trafo were wrongly permuted according to the COLUMN permutation introduced by the QR
  factorisation with column pivoting

- Remove spurious *
  ([`a871d39`](https://gitlab.com/chilton-group/angmom_suite/-/commit/a871d39104aa7333eb85784a131c06ad0d6efd7e))

- Units
  ([`8fb3071`](https://gitlab.com/chilton-group/angmom_suite/-/commit/8fb3071590920eda5545c46089506c99284b93cb))

the units of chi are now following cgs-emu system

### Build System

- Add jax requirement
  ([`23e5363`](https://gitlab.com/chilton-group/angmom_suite/-/commit/23e536387b933a2114996a4ea09f59d4f95143f0))

add jax + jaxlib as dependencies

- Fix incorrect bugtracker and url entries
  ([`ecb0a6e`](https://gitlab.com/chilton-group/angmom_suite/-/commit/ecb0a6eb21f2c440d81d9e22a3a5302260ed91df))

- Update dependencies
  ([`4ee2b39`](https://gitlab.com/chilton-group/angmom_suite/-/commit/4ee2b391a4a55399ee47fd2bb39c849d75565e9a))

Remove molcas_suite and bump hpc_suite dependency.

### Features

- Add basic proj cli interface
  ([`feac085`](https://gitlab.com/chilton-group/angmom_suite/-/commit/feac085c25239a2226d40686e697a3ec12a3e1b7))

Spin Hamiltonian parameter projection cli interface which will replace cfp cli interface. Still
  misses some advanced features like quax and B-field application.

- Add S and S^k calculation
  ([`0c8a560`](https://gitlab.com/chilton-group/angmom_suite/-/commit/0c8a5603021fd994663595445364a525cd91556f))

- Add strength within order
  ([`6a8a948`](https://gitlab.com/chilton-group/angmom_suite/-/commit/6a8a9488dc2fa5e319f66b38ee0dc817de1772b6))

- Add z-projection of total angular momentum
  ([`20a346e`](https://gitlab.com/chilton-group/angmom_suite/-/commit/20a346ea73f69bab079ee7bc3d754e6c1cf64c8a))

Now prints z-proj of uncoupled angular momentum operators alongside energies.

- Add zeeman splitting of model states
  ([`3c4e892`](https://gitlab.com/chilton-group/angmom_suite/-/commit/3c4e892e695d20d7b7bc4b8f10035cc9888ecf17))

Zeeman Hamiltonian can be turned on through --field argument.

- Chi*t simulation, wip
  ([`1a3f0a8`](https://gitlab.com/chilton-group/angmom_suite/-/commit/1a3f0a86e756b70467faaa33d2b6b9509560855b))

Initial implementation of susceptibility.

- Differential susceptibility
  ([`cd63eaa`](https://gitlab.com/chilton-group/angmom_suite/-/commit/cd63eaa0f8c89ba6ddf0215b3549dbdc702a8998))

implement differential susceptibility at zero field

- Implement g-tensor/factor, wip
  ([`2c59f7b`](https://gitlab.com/chilton-group/angmom_suite/-/commit/2c59f7b95c00a04297082f12a1b9cdcbd13f81e2))

The model program now returns g-tensor/factors. To be cleaned up

- Implementation of scale argument
  ([`73bd993`](https://gitlab.com/chilton-group/angmom_suite/-/commit/73bd993c9c7cb3f7dc22caef089acdf8a3081665))

Facilitates the scaling of whole spin Hamiltonian terms by factor.

- Implementation of SO3 irrep count
  ([`c8eaa25`](https://gitlab.com/chilton-group/angmom_suite/-/commit/c8eaa25d2034ed414d259cec1f95b9f05a2a14e6))

SO3 irrep count by character orthogonality.

- Improve quax argument
  ([`5fe46b2`](https://gitlab.com/chilton-group/angmom_suite/-/commit/5fe46b2d844755a3432d3cb0ccbef608fbdad793))

QUAX can be chosen by x, y, z as well as loaded from plain txt

- Initial implementation of hamiltonian program
  ([`b2ff5d1`](https://gitlab.com/chilton-group/angmom_suite/-/commit/b2ff5d12d5016c796a154e49d1f4c34583194631))

The hamiltonian program can be employed to parametrise a model Hamiltonian with multiple sets of
  parameters.

- Lzee and Szee terms for generalised Zeeman Hamiltonians
  ([`39e2566`](https://gitlab.com/chilton-group/angmom_suite/-/commit/39e2566ec53ae2e222f11011372775f87bbb92cd))

Lzee and Szee terms can be supplied with --field to generate Zeeman splitting due to general spin
  and orbital angular momenta.

- State/term composition
  ([`3b674fc`](https://gitlab.com/chilton-group/angmom_suite/-/commit/3b674fc9d4d29a3bd2c0a2b00f49b03c8e6019b1))

Backend for integrating SO3 over all group elements.

- Support quax keyword
  ([`acb1dd6`](https://gitlab.com/chilton-group/angmom_suite/-/commit/acb1dd6eedd2f3e216495d7117ce3a4d7708f1ff))

quax can be chosen to perform angular momentum quantisation along specific axis.

### Performance Improvements

- Cache cg_trafo
  ([`8a21266`](https://gitlab.com/chilton-group/angmom_suite/-/commit/8a21266b6ab1475eb9dc1704e85b1d81b1bd92f3))

Block CG trafo computation involves the repeated calculation of the same CG-coefficient matrices.
  Caching them improves performance.

- Jit susceptibilities
  ([`7a8c02d`](https://gitlab.com/chilton-group/angmom_suite/-/commit/7a8c02dcaae47cdeed0766f868ea78c36f86889c))

slight improvements, but not avoiding multiple evaluations of the matrix exponential at different
  temperatures.

- Performance improvements for chi
  ([`241ac6d`](https://gitlab.com/chilton-group/angmom_suite/-/commit/241ac6dd1b8d30d0a549e74ee98f8b20a3b0b64c))

For conventional chi the partition function is based on the eigen values of the hamiltonian which
  does not have to be recalculated for different temperatures. Differential chi still based on trace
  of expm which has to be recalculated for different temps.

- Return model operators in iterators
  ([`c996d3e`](https://gitlab.com/chilton-group/angmom_suite/-/commit/c996d3ee58cc79ceb353368249533633126cf511))

Model operators are retuned as iterators to evaluate them lazyly which improves memory overhead with
  large model spaces.

- So3 integration fully analytical
  ([`87bcf08`](https://gitlab.com/chilton-group/angmom_suite/-/commit/87bcf08cd0a4350e3dfc3bde9dac0e33307a47ef))

Usage of "block-triangular" method for all three integrals.

### Refactoring

- Adapt project_CF to new backend
  ([`911b192`](https://gitlab.com/chilton-group/angmom_suite/-/commit/911b1925ca5cf4ae27051066b8417649f052a620))

Project_CF is going to be rewritten to serve as general implementation of spin Hamiltonian parameter
  projection.

- Adapted zeeman input
  ([`08e37a1`](https://gitlab.com/chilton-group/angmom_suite/-/commit/08e37a18f5d90708c071914e7162f55f3b0bd8bd))

Zeeman Hamiltonian can now be calculated along arbitrary axis.

- Angmom SO(3) projection
  ([`f03c2b6`](https://gitlab.com/chilton-group/angmom_suite/-/commit/f03c2b6ebe9465a03beac379521a6c9a6fecba88))

base angmom basis evaluation on SO(3) projection operator

- Base basis evaluation on real arithmetic
  ([`8e50b31`](https://gitlab.com/chilton-group/angmom_suite/-/commit/8e50b310efd983e467c7c2d6b635cf8ece7dc051))

Using real arithmetic in Jz diagonalisation might nicely define phase.

- Clean up chi implementation
  ([`9d61fb1`](https://gitlab.com/chilton-group/angmom_suite/-/commit/9d61fb142f65600cdc8f4363fbbeb174a4158402))

remaining (i) investigate field dependence of chi (ii) get differential chi to work

- Clean up coupling code
  ([`81c84f2`](https://gitlab.com/chilton-group/angmom_suite/-/commit/81c84f26cc9d7d575cbfb590a4cdf5cb8843e936))

General clean up to retain canonical qn ordering during coupling. Also resolves some issues during
  symbol parsing.

- Clean up projection code
  ([`9ab972d`](https://gitlab.com/chilton-group/angmom_suite/-/commit/9ab972def9ae25f5973c2c89c38e29d9055517dc))

Simplify code and make more pythonic. Also add amfi relative phase fix.

- Generalise print_basis
  ([`dac09e3`](https://gitlab.com/chilton-group/angmom_suite/-/commit/dac09e31c43c58db02cf092395845ecf3841ba11))

Now prints arbitrary operators

- Iso_soc only includes first order
  ([`7847b65`](https://gitlab.com/chilton-group/angmom_suite/-/commit/7847b65abad34ef5e8c7560580c7e04a6dfbb9ba))

Switch from power series of isotropic soc operators to first order only.

- Remove unused code
  ([`0ad318c`](https://gitlab.com/chilton-group/angmom_suite/-/commit/0ad318c912c2545745520f6f3e6ec03e2f65efd1))

remove old block diagonalisation routine and other unused routines.

- Spin Hamiltonian projection
  ([`6e3d1d3`](https://gitlab.com/chilton-group/angmom_suite/-/commit/6e3d1d334b70dacd07998b438161730aef4a64cd))

Refactor SHP to be more straight forward and set up hpc_suite Store facility.

- Standardise parameter naming
  ([`53b113b`](https://gitlab.com/chilton-group/angmom_suite/-/commit/53b113b401cf92484a0ca5be08d9317c779b0cd0))

Keys of spin Hamiltonian parameters are now named in a more uniform way.

- Stream-line basis trafo
  ([`65748d9`](https://gitlab.com/chilton-group/angmom_suite/-/commit/65748d94aff2da902b44dff0ac34fcc2eaba158d))

Basis transformation and printing is now separated. Removal of imaginary relative phase between
  angmom irreps cleaned up.

- Stream-line basis trafo function, wip
  ([`37819a4`](https://gitlab.com/chilton-group/angmom_suite/-/commit/37819a427cbaba06d49f80e7d6971ad4fc9b59c0))

1.) always initially transform to fully coupled basis 2.) more consistent coupling repr 3.) phasing
  functions


## v1.9.0 (2022-06-13)

### Build System

- Resolve circular import
  ([`2b0612b`](https://gitlab.com/chilton-group/angmom_suite/-/commit/2b0612b69b739ea784aa4948ed2a3548a710ed91))

Circular import from molcas_suite triggered issues. Now quax action is overwritten when quax input
  is used molcas_suite otherwise quax needs to be read from pre-extracted h5 file.


## v1.8.2 (2022-06-09)

### Build System

- Activate docs pipeline only when version number updated
  ([`10b0437`](https://gitlab.com/chilton-group/angmom_suite/-/commit/10b04372a3146db479f340cc484d96df62172eb6))

- Switch to pypi for distribution
  ([`1e40775`](https://gitlab.com/chilton-group/angmom_suite/-/commit/1e40775b9fbb5768256674ee026f3445993bee33))


## v1.8.1 (2022-05-19)

### Bug Fixes

- Ion parser
  ([`4ca435c`](https://gitlab.com/chilton-group/angmom_suite/-/commit/4ca435c34b3ea41864db0ba233284ce532710f6d))

forgot to amend last time.


## v1.8.0 (2022-05-19)

### Bug Fixes

- Remove normalisation from radical operators
  ([`eae0660`](https://gitlab.com/chilton-group/angmom_suite/-/commit/eae06607a450627485cb4b1073f25469e4dd2c39))

now matches definitions from gould2022

### Features

- Expand model options
  ([`df28fbe`](https://gitlab.com/chilton-group/angmom_suite/-/commit/df28fbeb87f0f0cc331a2b54ae143347bf605156))

now supports anisotropic soc and even/odd time reversal symmetric operators.

- Theta factor support for all lanthanides
  ([`14aafd0`](https://gitlab.com/chilton-group/angmom_suite/-/commit/14aafd03c3d1b50409db1f31b32d20cbf7b5c881))

Include full tables from phi manual.

### Refactoring

- Improve output
  ([`fd26860`](https://gitlab.com/chilton-group/angmom_suite/-/commit/fd26860be423d81ba3aa32b9ed97ef4690cfe7e4))

improve error printing and parameter labels


## v1.7.5 (2022-04-11)

### Bug Fixes

- Anisotropic soc operator
  ([`6f3f4f0`](https://gitlab.com/chilton-group/angmom_suite/-/commit/6f3f4f0d7e3cdfa2919e9d3bff0cdae7e5d7232d))

Remove extra call to sum.

- Phasing bug
  ([`0189037`](https://gitlab.com/chilton-group/angmom_suite/-/commit/018903792f2917659684a0327f4f4c4ed461dd8c))

Wrong blocks were subset for phasing.

### Refactoring

- Improve model error printing
  ([`ab9d079`](https://gitlab.com/chilton-group/angmom_suite/-/commit/ab9d079c0f139187d748c2cae8d5f7e5bab85643))

Now prints euclidean distance between model and fit not square.


## v1.7.4 (2022-04-06)

### Bug Fixes

- Define basis labels if no coupled angm
  ([`2c3d0b0`](https://gitlab.com/chilton-group/angmom_suite/-/commit/2c3d0b011af475179e5ad52e8cc6da3be47f34dd))

Basis labels need to be initialised to None if no coupled angmom operators.

### Build System

- Add molcas_suite requirement
  ([`a686f9a`](https://gitlab.com/chilton-group/angmom_suite/-/commit/a686f9aa3766a7d1504784acbf3903b69d1829bb))

append molcas_suite version 1.11.0 to the requirements in setup.py


## v1.7.3 (2022-04-05)

### Bug Fixes

- Adjust units in zeeman hamiltonian
  ([`8a6ea44`](https://gitlab.com/chilton-group/angmom_suite/-/commit/8a6ea444df2179e88097fb7bb58bcd03e45533ae))

Zeeman hamiltonian is now computed in atomic units.

- Type in setup.py
  ([`874c06d`](https://gitlab.com/chilton-group/angmom_suite/-/commit/874c06ddfa8347f94611ba994785994bf2561498))

missing comma in requirements list.


## v1.7.2 (2022-04-05)

### Bug Fixes

- Carry over blk labs
  ([`3edf873`](https://gitlab.com/chilton-group/angmom_suite/-/commit/3edf87365a68f1398e24485d92824872df74fe64))

Block labels are now being carried over from the coupled to the uncoupled trafo.

- Fix and improve term symbol parsing/printing
  ([`448e236`](https://gitlab.com/chilton-group/angmom_suite/-/commit/448e236c6184148c6dc2a156d3a5f28ff0f411fb))

Converting level symbols to strings accidentally was using S instead of J to label total angmom.

### Build System

- Add hpc_suite requirement
  ([`581ef15`](https://gitlab.com/chilton-group/angmom_suite/-/commit/581ef1554fadeaffbf753bc41ef4ac707e9ab24a))

Add hpc_suite version 1.2.0 to requirementes in setup.py.


## v1.7.1 (2022-04-04)


## v1.7.0 (2022-04-04)

### Bug Fixes

- Fix op_multiply
  ([`fa1c832`](https://gitlab.com/chilton-group/angmom_suite/-/commit/fa1c832609c0230e58ec9d461a1e96f0d24bbb66))

Bug in operator matrix multiply function

- Fix op_multiply
  ([`8655662`](https://gitlab.com/chilton-group/angmom_suite/-/commit/8655662858500381fc758fdcf416458b3e821e3b))

Bug in operator matrix multiply function

- Sign bug in exchange Hamiltonian
  ([`abdb90b`](https://gitlab.com/chilton-group/angmom_suite/-/commit/abdb90ba11989f8dcca6f157047f1c8ad60e0b9d))

The negative sign was put in front of every Stevens operator, cancelling in the bipartite terms.
  Moves sign in front of exchange term.

- Sign bug in exchange Hamiltonian
  ([`68e6799`](https://gitlab.com/chilton-group/angmom_suite/-/commit/68e679962b84a3a529a99dc4c7ae9055d49da76a))

The negative sign was put in front of every Stevens operator, cancelling in the bipartite terms.
  Moves sign in front of exchange term.

### Features

- Feat: add preconditioning of Kramers doublets
  ([`3201f30`](https://gitlab.com/chilton-group/angmom_suite/-/commit/3201f30443db513d02a242a611692fae4bb3fa4b))

Kramers doublets can be either split by explicit magnetic field or rotated into eigenstates of Jz at
  zero field.

- Feat: add preconditioning of Kramers doublets
  ([`4652ac7`](https://gitlab.com/chilton-group/angmom_suite/-/commit/4652ac7dacb8164e1f082797fb540d1ea7f97dd7))

Kramers doublets can be either split by explicit magnetic field or rotated into eigenstates of Jz at
  zero field.

### Refactoring

- Handle CG trafo as method of Symbol class
  ([`536e5fe`](https://gitlab.com/chilton-group/angmom_suite/-/commit/536e5fe3e6d785825b837430aa62698d22d41333))

Also, clean up definitions for block extraction and remove deprecated code.

- Handle CG trafo as method of Symbol class
  ([`71eba10`](https://gitlab.com/chilton-group/angmom_suite/-/commit/71eba102e4cb8f40c40d6c93d4f3caadf57bc953))

Also, clean up definitions for block extraction and remove deprecated code.


## v1.6.2 (2022-03-31)

### Refactoring

- Clean up basis and cfp code AGAIN
  ([`873f132`](https://gitlab.com/chilton-group/angmom_suite/-/commit/873f13279bffbf3ad8392f1222697d1c546091c6))

Now everything plays even better with the Symbol classes.


## v1.6.1 (2022-03-30)

### Build System

- Add h5py dependency
  ([`21da8e4`](https://gitlab.com/chilton-group/angmom_suite/-/commit/21da8e4c61a14314bbf2bf7ff98b21bf07d7ffb5))

Add missing h5py dependency to setup.py


## v1.6.0 (2022-03-30)

### Bug Fixes

- Fix cg-decomposition
  ([`f4b042f`](https://gitlab.com/chilton-group/angmom_suite/-/commit/f4b042f60b075f39bb2fd3fcbea267ab7b2cded3))

Change CG decomposition function into working condition. Now acts on angular momentum states not
  CASSCF roots, which actually makes sense ...

### Continuous Integration

- Add `if` to avoid pipelines triggering on all branches
  ([`01a0e78`](https://gitlab.com/chilton-group/angmom_suite/-/commit/01a0e78d80fe9a99514de2ff389efe9fe35437e8))

- Switch to chiltron
  ([`c7f7f28`](https://gitlab.com/chilton-group/angmom_suite/-/commit/c7f7f28f4e0a766e61110dbb6b877e3cd977b758))

### Features

- Cfp option parser and quax action
  ([`fbb468c`](https://gitlab.com/chilton-group/angmom_suite/-/commit/fbb468cafe9920a372c1782c59eee00f9e472a5d))

Add modern parser for CFP options to be used by "molcas_suite cfp" and "spin_phonon-suite
  derivatives" lvc function specification.

- Cg_decomposition function
  ([`a992b49`](https://gitlab.com/chilton-group/angmom_suite/-/commit/a992b49046c4454dfc4fe8079c1cb0612818f954))

Functionality to decompose coupled angular momenta into uncoupled angular momenta.

- Generalise basis trafo
  ([`9dde724`](https://gitlab.com/chilton-group/angmom_suite/-/commit/9dde724f602f217c0bf90f0230ee31eb0afc7343))

Generalise angm basis transformation to accommodate extra angmom operators.

- Improvement of CF projection code
  ([`4b41662`](https://gitlab.com/chilton-group/angmom_suite/-/commit/4b4166258a8317ec64ac4a0b5b37685c7cc86e52))

Now takes input space in terms of term symbols to deduce blk_count.

- State composition
  ([`a9563a5`](https://gitlab.com/chilton-group/angmom_suite/-/commit/a9563a59a72544ff2732b3abe3eeea0d03a3fb63))

Print input state composition.

### Refactoring

- Angular momentum assignment
  ([`b31bc3f`](https://gitlab.com/chilton-group/angmom_suite/-/commit/b31bc3f03fae75e7b3beba41a32dbed4f4d918db))

Carry out angmom assignment purely based on counts. WIP

- Clean up cfp projection
  ([`5bf6cb5`](https://gitlab.com/chilton-group/angmom_suite/-/commit/5bf6cb5d4c302804da3e2a534dda53a3c7cdabfc))

Clean up implementation to be used by molcas_suite cfp program.

- Experiments with basis assignment
  ([`f3f5774`](https://gitlab.com/chilton-group/angmom_suite/-/commit/f3f5774eda0576fd2ad62b6ebf22c6c51302be3b))

Use new method to carry out basis assignment, not working.

- Generalise model Hamiltonian projection
  ([`1bea3cb`](https://gitlab.com/chilton-group/angmom_suite/-/commit/1bea3cb53bc2d20e99d243c0485d45b05110d15d))

Add general SpinHamiltonian class with high flexibility.

- Improve basis classification
  ([`ec941ea`](https://gitlab.com/chilton-group/angmom_suite/-/commit/ec941eaf28eb039759b86728e121d5a29a6552d7))

basis is now classified per block.

- Improvement of angmom basis assignment
  ([`82a2943`](https://gitlab.com/chilton-group/angmom_suite/-/commit/82a29439f8caac5a823e214abcd06844992e5dc6))

Basis label assignment is now done via Needleman-Wunsch type algorithm using Jx matrix alignment.

- Major refactor of angmom basis trafo and cfp projection
  ([`66acd71`](https://gitlab.com/chilton-group/angmom_suite/-/commit/66acd7122e8aa9486485d69e911136bd7cef97fd))

Any basis transformation is carried out by first transforming to the uncoupled basis and
  subsequently to the coupled basis if requested.

- Minor clean up
  ([`2065fc0`](https://gitlab.com/chilton-group/angmom_suite/-/commit/2065fc020648ab8426a9649c7f4cbe4bdec56b84))

remove some tuple constructors.

- Pull out angm rotation
  ([`f6662ee`](https://gitlab.com/chilton-group/angmom_suite/-/commit/f6662ee75bfc8e20762d62f3aa9ead2f7446712e))

modularise

- Reorganise code
  ([`7cde66d`](https://gitlab.com/chilton-group/angmom_suite/-/commit/7cde66d5161d124ee469c8b424b600c494a4b71a))

move angular momentum function from crystal.py to basis.py for better dependency management.

- Wip
  ([`2ff5ede`](https://gitlab.com/chilton-group/angmom_suite/-/commit/2ff5ede6f934663a9331761e5fd7ee986efd4c0a))


## v1.5.0 (2022-02-15)

### Features

- Return basis labels from angmom basis trafo
  ([`9c129c4`](https://gitlab.com/chilton-group/angmom_suite/-/commit/9c129c404e33bcc2d7cd306187519d69085ad125))

Angmom basis labels are now returned from find_angm_basis() as a third argument.


## v1.4.0 (2022-02-15)

### Features

- Return basis labels from angmom basis trafo
  ([`560d838`](https://gitlab.com/chilton-group/angmom_suite/-/commit/560d8388dc5e192478b05cf194c9dc2d64e06d6c))

Angmom basis labels are now returned from find_angm_basis() as a third argument.


## v1.3.0 (2022-02-14)

### Refactoring

- Separate angm basis trafo from CFP projection
  ([`9b746fd`](https://gitlab.com/chilton-group/angmom_suite/-/commit/9b746fd9b63e490e619461ad4e241985be0da7c1))

The angmom basis transformation functionality can now be used independently of the CFP projection
  code.


## v1.2.1 (2022-02-09)

### Build System

- Update dependencies
  ([`d842952`](https://gitlab.com/chilton-group/angmom_suite/-/commit/d842952b1ae7f1746b3943f5814f655a2492da7e))

restrict scipy version to enable correct version of pinvh function.

### Refactoring

- Merge stevens.py into crystal.py
  ([`719504e`](https://gitlab.com/chilton-group/angmom_suite/-/commit/719504e2cb73df708a97250c3534ba12bd6453cf))

Merge Stevens ops projection functonality into crystal submodule.


## v1.2.0 (2022-02-09)

### Documentation

- Updated readme
  ([`cddd0c6`](https://gitlab.com/chilton-group/angmom_suite/-/commit/cddd0c6d9c56f452893ad97a76e86cf6d9c52d51))

### Features

- Migrate steven op projection code
  ([`529f004`](https://gitlab.com/chilton-group/angmom_suite/-/commit/529f00431a63637a8d1eb5848ac28319ec1aa607))

Move stevens operator projection code from spin-phonon_suite to here.


## v1.1.0 (2022-02-08)

### Documentation

- Add header
  ([`b7a9223`](https://gitlab.com/chilton-group/angmom_suite/-/commit/b7a922328d783066a17a02b33661eb94048fcc6d))

- Add headers to submodules
  ([`9c52a1b`](https://gitlab.com/chilton-group/angmom_suite/-/commit/9c52a1b83f436d895b6846eac06cadc114e6f3a6))

- Update docstring formatting
  ([`a37fd4b`](https://gitlab.com/chilton-group/angmom_suite/-/commit/a37fd4bc85268320ede1311061725e0f815cbae7))

### Features

- Updates kq_to_num code
  ([`141f1da`](https://gitlab.com/chilton-group/angmom_suite/-/commit/141f1da977f07d826764f5cb82b523628dddc9aa))

### Refactoring

- Refactor code into more sensible arrangements
  ([`3e2bf1a`](https://gitlab.com/chilton-group/angmom_suite/-/commit/3e2bf1a8defdcb2a5b9d1b6f4f7145ae40322da6))


## v1.0.0 (2022-02-08)

### Chores

- Further initial commits
  ([`0520b97`](https://gitlab.com/chilton-group/angmom_suite/-/commit/0520b97281e57e4038658dd03a6faf17875cc6bd))

- Initial commit of inorgqm code
  ([`222f55f`](https://gitlab.com/chilton-group/angmom_suite/-/commit/222f55f02ad6f4b2e430e9237aba52aabac41d7b))

### Continuous Integration

- Change master to main
  ([`904ae28`](https://gitlab.com/chilton-group/angmom_suite/-/commit/904ae28a6d8bc985ec6b20a4082b4ae88f8fe156))

- Remove `cd package`
  ([`ed774f0`](https://gitlab.com/chilton-group/angmom_suite/-/commit/ed774f01ea3bc06f9fc1bd10418b597d828e0f45))
